<?php
/**
 * @file
 * Contains \Drupal\json_list\Controller\JsonController.
 */

namespace Drupal\post_list\Controller;

use Drupal\Core\Controller\ControllerBase;

class PostController extends ControllerBase {
  public function content() {
    
    $posts = file_get_contents("https://jsonplaceholder.typicode.com/posts");
    $posts = json_decode($posts, true);

    $users = file_get_contents("https://jsonplaceholder.typicode.com/users");
    $users = json_decode($users, true);

    return [
        '#theme' => 'post_list',
        '#posts' => $posts,
        '#users' => $users
    ];
  }
}